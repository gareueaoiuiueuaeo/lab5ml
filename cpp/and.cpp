#include <iostream>
#include <vector>
#include <string>
#include <cmath>
#include <fmt/core.h>

#define matrixDouble vector<vector<long double>>

using namespace std;

matrixDouble sigmoid (const matrixDouble& m1){
	matrixDouble ans = m1;
	for(int i = 0; i < ans.size(); i++){
		for(int j = 0; j < ans[0].size(); j++){
			ans[i][j] = 1/(1+expl(-m1[i][j]));
		}
	}
	return ans;
}

//function to print the matrix
void print(vector<vector<long double>> vecA){
	for (auto a :vecA){
		for (auto b : a)
			cout << b << " ";
		cout << "\n";
	}
	cout << "\n";
}


//function to multiply matrix
void multiplie(vector<vector<long double>> vecA, vector<vector<long double>> vecB, vector<vector<long double>> &ans){
	
	for(int i = 0; i < vecA.size(); i++){
		for(int j = 0; j < vecB[0].size(); j++){
			for(int k = 0; k < vecA[0].size(); k++){
				ans[i][j] += vecA[i][k] * vecB[k][j];
			}
		}
	}
}

//operator + overload
vector<vector<long double>> operator+(const vector<vector<long double>>& m1, const vector<vector<long double>>& m2){
	vector<vector<long double>> ans(m1.size(), vector<long double>(m1[0].size()));
	
	if(m1.size() != m2.size() && m1[0].size() != m2[0].size())
		return ans;
	
	for (int i = 0;i < m1.size();i++) {
		for(int j =0; j < m1[0].size(); j++){
			ans[i][j] = m1[i][j]+m2[i][j];
		}
	}	
	return ans;
}

//operator - overload
vector<vector<long double>> operator-(const vector<vector<long double>>& m1, const vector<vector<long double>>& m2){
	vector<vector<long double>> ans(m1.size(), vector<long double>(m1[0].size()));
	if(m1.size() != m2.size() && m1[0].size() != m2[0].size())
		return ans;
	for (int i = 0;i < m1.size();i++) {
		for(int j =0; j < m1[0].size(); j++){
			ans[i][j] = m1[i][j]-m2[i][j];
		}
	}	
	return ans;
}

//operatar * overload
vector<vector<long double>> operator*(const vector<vector<long double>>& m1, const vector<vector<long double>>& m2){
	vector<vector<long double>> ans(m1.size(), vector<long double>(m2[0].size()));
	if(m1[0].size() == 1 && m2[0].size() == 1 && m1.size() == m2.size()){
		for (int i = 0;i < m1.size();i++) {
			ans[i][0] = m1[i][0]*m2[i][0];
		}
		return ans;
	}
	for(int i = 0; i < m1.size(); i++){
		for(int j = 0; j < m2[0].size(); j++){
			for(int k = 0; k < m1[0].size(); k++){
				ans[i][j] += m1[i][k] * m2[k][j];
			}
		}
	}
	return ans;
}

vector<vector<long double>> operator*(const vector<vector<long double>>& m1, const int m2){
	vector<vector<long double>> ans(m1.size(), vector<long double>(m1[0].size()));
	for (int i = 0;i < m1.size();i++) {
		for(int j =0; j < m1[0].size(); j++){
			ans[i][j] = m1[i][j]*m2;
		}
	}	
	return ans;
}


//fonction to set the matrix
void setMatrix(vector<vector<long double>> &vecA, int M, int N, string MatrixName){
	for (int i = 0;i < M;i++) {
		for(int j =0; j < N; j++){
			cout << fmt::format("enter {} {} elemet of matrix {} ", i, j, MatrixName);
			cin >> vecA[i][j];
		}
	}
}

//matrix transponation3
vector<vector<long double>> Transponate(vector<vector<long double>>& m1){
	vector<vector<long double>> temp(m1[0].size(), vector<long double>(m1.size()));
	
	for(int i = 0; i < m1.size(); i++){
		for (int j = 0; j < m1[0].size(); j++){
			temp[j][i] = m1[i][j];
		}
	}
	return temp;
}




int main(int argc, char *argv[]) {
	matrixDouble X = {	{0,0,0},
						{0,1,1},
						{1,0,0},
						{1,0,1},
						{1,1,1}
						};
						
	matrixDouble Y  = {{1,0,1,0,1}};
	matrixDouble YT = Transponate(Y);
	matrixDouble Syn = {{-0.73785167},{0.69892666},{0.54330635}};


	matrixDouble layer0 = X;
	matrixDouble a = layer0 * Syn;
	matrixDouble layer1 = sigmoid(a);
	matrixDouble layer1_error = YT - layer1;
	matrixDouble layer1_delta = layer1_error * sigmoid(layer1);
	Syn = Syn + (Transponate(layer0) * layer1_delta);

	
	for(int i = 0; i < 100; i++){
		layer0 = X;
		a = layer0*Syn;
		a = layer0 * Syn;
		layer1 = sigmoid(a);
		layer1_error = YT - layer1;
		layer1_delta = layer1_error * sigmoid(layer1);
		Syn = Syn + (Transponate(layer0) * layer1_delta);
		
		
		
////		cout << fmt::format("layer0 {0} \na {1} \nlayer1 {2} \nlayer1_error {3} l\neyear_delta {4} \nsyn {5}", 12,21,21,12,12,2);
//		cout << "i " << i << endl;
//		cout << "layer0 " << endl; print(layer0);
//		cout << "a " << endl; print(a);
//		cout << "layer1 " << endl; print(layer1);
//		cout << "layer1_error " << endl; print(layer1_error);
//		cout << "sigmoid(layer1) " << endl;print(sigmoid(layer1));
//		cout << "leyear1_delta " << endl; print(layer1_delta);
//		cout << "Syn " << endl; print(Syn);
//		cout << "==============================================================================" << endl;
//		cout << "==============================================================================" << endl;

		
		
		
	}
	
	print(Syn);
	print(a);
	
	matrixDouble ns = {{1,1,1}};
	print(ns);
	print(Syn);
	print(ns*Syn);
	print(sigmoid(ns * Syn));	
}