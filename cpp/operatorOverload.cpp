#include <iostream>

using namespace std;


class testClass{
	int a;
	int b;
	public:
		testClass();
		testClass(int setA, int setB){
			this->a = setA;
			this->b = setB;
		}
		testClass operator=(testClass &te){
			this->a = te.a;
			this->b = te.b;
		}
		inline bool operator==(testClass& te1){
			if(this->a == te1.a && this->b == te1.b)
				return true;
			return false;
		}
};

int main(int argc, char *argv[]) {
	testClass a(1,1);
	testClass b = a;
	int c = a==b ;
	cout << (a==b);
}