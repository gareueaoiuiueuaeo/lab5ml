#!/usr/bin/python

import numpy as np

# sigmoid function
def sigmoid(x,deriv=False):
	return 1/(1+np.exp(-x))
	
# input dataset
X = np.array([  [0,0,0],
				[0,0,1],
				[0,1,0],
				[0,1,1],
				[1,0,0],
				[1,0,1],
				[1,1,0],
				[1,1,1] 
				])
	
# output dataset            
y = np.array([[0,1,0,1,0,1,1,1]]).T

#random seed, optional
np.random.seed()

# initialize weights randomly
syn0 = 2*np.random.random((3,1)) - 1	
for iter in xrange(10000):

	# forward propagation
	layer0 = X
	a = np.dot(layer0,syn0)
	layer1 = sigmoid(a)

	# eror
	layer1_error = y - layer1

	# multiply how much we missed by the 
	# slope of the sigmoid at the values in layer1
	layer1_delta = layer1_error *  sigmoid(layer1)
	# update weights
	syn0 += np.dot(layer0.T,layer1_delta)


print(syn0)

ns = np.array([1,0,0])

print(sigmoid(np.dot(ns, syn0)))



